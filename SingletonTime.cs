﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreadLocksMonitor_hw
{
    public class SingletonTime
    {
        private static SingletonTime _instance;
        private static object key = new object();
        private SingletonTime()
        {

        }

        public void GetTime(string log)
        {
            Console.WriteLine($"Time is: {DateTime.Now}");
        }

        public static SingletonTime GetInstance()
        {
            if (_instance == null)
            {
                lock (key)
                {
                    if (_instance == null)
                    {
                        _instance = new SingletonTime();
                    }
                }
            }
            return _instance;
        }
    }
}
